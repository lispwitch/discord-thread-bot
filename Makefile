.PHONY: log

run:
	./run.sh

log:
	tail -F logs/`ls -t1 logs | head -n 1`

pid:
	pgrep -f botboy && echo $?

kill:
	kill -2 `pgrep -f botboy`
