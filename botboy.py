"""
A thread bot for BYOB
"""

from typing import NoReturn
from pathlib import Path
from discord.ext import commands
from dotenv import load_dotenv
from datetime import datetime, timezone

import os
import asyncio
import discord
import logging

from settings import BotBoyState

logging_path = Path('.', 'logs')
config_path = Path(os.getenv('HOME'), '.threadbot.json')
intro_message = Path('.', 'intro_message')

log = logging.getLogger('discord')
state = BotBoyState(log, config_path)
help_command = commands.DefaultHelpCommand(dm_help=True,
                                           no_category="Commands are")
client = commands.Bot(command_prefix='.',
                      description="Chill bot for threads. Use .help for more info",
                      help_command=help_command)


async def send_owner_message(guild: object):
    """Send the owner an introductory message after we join a guild"""
    if not intro_message.exists():
        return None
    with open(intro_message, "r") as h:
        message = h.read()
        owner = guild.owner
        if owner:
            await owner.send(message)


@client.event
async def on_guild_join(guild: object) -> NoReturn:
    """Things to do when we join a new discord server"""
    log.info(f"{guild.name}: We have been added the server!")
    state.add_guild(guild)
    state.save()
    await send_owner_message(guild)


@client.event
async def on_guild_remove(guild: object) -> NoReturn:
    """Things to do when we leave a discord server"""
    log.info(f"{guild.name}: We have been removed from server!")
    state.remove_guild(guild)
    state.save()


async def synchronize_guild_listings():
    """Synchronize the bot state, in case the database is out of date"""
    # Yes this is inefficient as Heck
    bot_ids = [guild.id for guild in client.guilds]
    stored_ids = state.list_guild_ids()

    log.info("Synchronizing guilds...")

    # if a bot id is not in the stored ids, we need to add it to the state
    add_guilds = [guild for guild in client.guilds
                  if guild.id not in stored_ids]

    for guild in add_guilds:
        log.info(f"Adding guild with id {guild.id}...")
        state.add_guild(guild)

    # if a stored id is not in the bot ids, we need to remove it from the state
    remove_guilds = [ident for ident in stored_ids
                     if ident not in bot_ids]

    for guild_id in remove_guilds:
        log.info(f"Removing guild with id {guild_id}...")
        state.remove_guild_by_id(guild_id)

    state.save()


@client.event
async def on_ready() -> NoReturn:
    """some async shizzle"""
    log.info(f'{client.user} has connected to Discord!')
    await synchronize_guild_listings()


@client.event
async def on_disconnect() -> NoReturn:
    """What to do on a disconnect"""
    log.info("Disconnect: Saving state.")
    state.save()


def make_thread_name(content: list) -> str:
    """Create a thread name"""
    args = content.split(" ")[1:]
    return "-".join(args).lower()


@client.command()
async def newthread(context: object) -> NoReturn:
    """Make a new thread: .newthread <threadname>\n"""
    guild = context.message.guild
    category = state.get_category(guild)
    if category >= 0:
        thread_name = make_thread_name(context.message.content)
        thread_delay = state.get_delay(guild)
        thread_category = discord.utils.get(guild.categories, id=category)
        await guild.create_text_channel(name=thread_name,
                                        category=thread_category,
                                        slowmode_delay=thread_delay)
    else:
        errstr = "No thread category has been set yet. Please do that first."
        await context.send(errstr)


async def update_posting_delays(context: object, delay: int) -> NoReturn:
    """Update the posting delays for the threads"""
    reason = f"Alter the thread posting delay to {delay}"
    guild = context.message.guild
    category = state.get_category(guild)
    if category >= 0:
        thread_category = discord.utils.get(guild.categories, id=category)
        async_tasks = [channel.edit(slowmode_delay=delay, reason=reason)
                       for channel in thread_category.text_channels]
        await asyncio.wait(async_tasks)
    else:
        errstr = "No thread category has been set yet. Please do that first."
        await context.send(errstr)


SET_DESCRIPTION = """
Change settings for the guild. Available settings are:
    .set category <category name ...>
        (Required) Tell the bot to place new threads in this category.

    .set delay <0 - 120>
        Tell the bot to update the default posting delay period for
        threads. Affects existing threads and new threads.
        The default posting delay is 30 seconds.

Settings available in a future version are:

    .set monitoring <true/false>
        Tell the bot to check for inactive channels and move them to a
        different category. As of the moment this has no effect due to
        lack of implementation :)
        The default is false.

    The following options are only available if monitoring inactivity
    is set to true:

    .set grace_period <seconds> <minutes> <hours> <days>
        Allow the channel to be inactive for the given amount of time
        before moving it. Available values are:

        seconds: 0 - 60
        minutes: 0 - 60
        hours: 0 - 24
        days: 0 - ...

        At the moment this has no default D:
        Throw me suggestions!

    .set archival_category <category name ...>
        The category to move inactive channels to.
-----
"""


@client.command(name="set", description=SET_DESCRIPTION)
@commands.has_permissions(manage_channels=True)
async def _set(context: object, key: str, *values: str) -> NoReturn:
    """Change one of the bot's settings"""
    guild = context.message.guild
    if key == "category":
        category_name = " ".join(v for v in values)
        message = f"Thread category has been set to {category_name}"
        # We can't use utils.get here because we need it to be case-insensitive.
        # Discord displays all category names in uppercase.
        category = discord.utils.find(lambda c: c.name.lower() == category_name.lower(),
                                      guild.categories)
        if not category:
            debug_info = "{values} - {category} not in {guild.categories}"
            log.warn(f"{guild.name}: Category does not exist; {debug_info}")
            await context.send("The category does not exist in the server")
            raise KeyError("Category does not exist!")

        state.set_category(guild, category.id)
        log.info(f"{guild.name}: " + message)
        await context.send(message)

    elif key == "delay":
        delay_int = int(values[0])
        if delay_int < 0 or delay_int > 120:
            log.warn(f"{guild.name}: Bad delay amount {values} -> {delay_int}")
            await context.send("Sorry, it needs to be a number between 0 - 120")
            raise ValueError(estr)

        message = f"Default posting delay set to {delay_int} seconds"
        state.set_delay(guild, delay_int)
        await update_posting_delays(context, delay_int)
        log.info(f"{guild.name}: " + message)
        await context.send(message)
    else:
        return


@_set.error
async def set_error(error: object, context: object):
    """The error stuff for settable values"""
    # TODO: find out why the heck other isinstance() stuff here doesn't actually
    # run on error
    if isinstance(error, commands.MissingPermissions):
        estr = "Woah man, not cool! You're not authorized to set this!!"
        await context.send(estr)

GET_DESCRIPTION = """
 View settings for the guild. Available settings are:
     .get category
         The category the point should place new threads in.

     .get delay (default: 30)
         The default posting delay period for threads.

     .get monitoring (default: false)
         Whether the bot should check for inactive channels and move them to
         a different category. As of the moment this is unimplemented.

 The following options are only available if monitoring inactivity
 is set to true:

     .get grace_period <seconds> <minutes> <hours> <days>
         The amount of time a channel can be inactive for before the bot
         moves it.

         At the moment this has no default D:
         Throw me suggestions!

     .get archival_category <category name ...>
         The category to move inactive channels to.
-----
"""


@client.command(name="get", description=GET_DESCRIPTION)
@commands.has_permissions(manage_channels=True)
async def _get(context: object, key: str) -> NoReturn:
    """Inspect the bot's settings"""
    guild = context.message.guild
    if key == "category":
        category_id = state.get_category(guild)
        if category_id > -1:
            category = discord.utils.get(guild.categories, id=category_id)
            message = f"Current thread category is set to: '{category.name}'"
        else:
            message = f"Category is currently unset"

        await context.send(message)

    elif key == "delay":
        delay = state.get_delay(guild)
        message = f"Current posting delay is set to {delay} seconds"
        await context.send(message)
    else:
        return


@_get.error
async def get_error(error: object, context: object):
    """The error stuff for settable values"""
    if isinstance(error, commands.MissingPermissions):
        estr = "Woah man, not cool! You're not authorized to get this!!"
        await context.send(estr)


def init():
    """literally just initialize things. stop complaining mr linter"""
    load_dotenv()
    token = os.getenv('DISCORD_TOKEN')
    return token

def logfile():
    isoformat = datetime.now(timezone.utc).isoformat()
    filename = isoformat + ".log"
    return Path(logging_path, filename)


def run():
    """run the damn thing"""
    log.setLevel(logging.INFO)
    handler = logging.FileHandler(filename=logfile(), encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    log.addHandler(handler)

    log.info("Grabbing token!!")
    token = init()
    state.load()
    client.run(token)


run()
